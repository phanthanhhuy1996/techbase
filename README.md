Introduction
---------------

![alt text](https://i.ibb.co/Rpvpfwp/Capture.png)

Getting Started
---------------

The easiest way to get started is to clone the repository and run these commands:

```bash
# Install NPM dependencies
npm install

# Then simply start your app
node app.js

# Run migration
migration up
```

API Document with Swagger
-------------
http://localhost:8080/api-docs/
![alt text](https://i.ibb.co/2yN3Hqk/Capture2.png)


Prerequisites
-------------
- [MongoDB](https://www.mongodb.com/download-center/community)
- [Node.js 10+](http://nodejs.org)
- https://www.npmjs.com/package/migrate-mongo
