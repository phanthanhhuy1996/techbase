const Employee = require('../models/Employee');
const Team = require('../models/Team');
const Department = require('../models/Department');

/**
 * GET /api/employee
 * List all employee.
 */
exports.getAllEmployee = async (req, res, next) => {
  Employee.find().populate({
    path : 'team',
    populate : {
      path : 'department'
    }
  }).exec({}, function (err, user) {  
    if (err) return next(err);
    res.send(user);
  })
};
