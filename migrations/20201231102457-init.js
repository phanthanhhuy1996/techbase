module.exports = {
  async up(db, client) {
    const department = await db.collection('departments').insertOne({ name: 'Security' })
    const team = await db.collection('teams').insertOne({ name: 'Team A', department: department.insertedId})
    const employee = await db.collection('employees').insertOne({ firstName: 'Phan Thanh', lastName: 'Huy', team: team.insertedId})
  },

  async down(db, client) {}
};