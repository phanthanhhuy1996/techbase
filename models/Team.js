const mongoose = require('mongoose');

const teamScheme = new mongoose.Schema({
  name: { type: String, unique: true },
  department: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Department' }],
}, { timestamps: true });

const Team = mongoose.model('Team', teamScheme);

module.exports = Team;
