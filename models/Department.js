const mongoose = require('mongoose');

const departmentScheme = new mongoose.Schema({
  name: { type: String, unique: true },
  manager: String
}, { timestamps: true });

const Department = mongoose.model('Department', departmentScheme);

module.exports = Department;
